#include <linux/kernel.h>
#include <linux/syscalls.h>

asmlinkage void sys_nop(void)
{
  return 0;
}
