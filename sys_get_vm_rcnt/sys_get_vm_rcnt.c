#include <linux/kernel.h>
#include <linux/syscalls.h> 
#include <asm/xen/hypercall.h>

#define XEN_RETRIEVE_COUNT 0

/* Retrieves the domain restore counter from the hypervisor */
asmlinkage long sys_get_vm_rcnt(void)
{
  printk("Checking restore counter in Xen hypervisor\n");  
  return HYPERVISOR_domain_restore_count(XEN_RETRIEVE_COUNT);
  //return HYPERVISOR_xen_version(0, NULL);
}
