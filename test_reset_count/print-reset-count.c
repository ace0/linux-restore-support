#include <linux/unistd.h>
#include <sys/syscall.h>
#include <stdio.h>

// 32-bit syscall number
//#define __NR_get_vm_rcnt 350

// 64-bit syscall number
#define __NR_get_vm_rcnt 313

/* Call the get-VM-reset-count system call. */
long get_reset_count()
{
  return syscall(__NR_get_vm_rcnt);
}

/** Print the VM reset count. */
int main()
{
  int cnt = get_reset_count();
  printf("Current reset count: %d\n", cnt);
  return 0;
}


